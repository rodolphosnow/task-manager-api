require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { build(:user) }

	it { is_expected.to have_many(:tasks).dependent(:destroy) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to validate_confirmation_of(:password) }
  it { is_expected.to validate_uniqueness_of(:auth_token) }

  describe '#info' do
    it 'returns emails and created_at and Token' do
      user.save!
      allow(Devise).to receive(:friendly_token).and_return('abc12345678')

      expect(user.info).to eq("#{user.email} - #{user.created_at} - Token: abc12345678")
    end
  end

  describe '#generate_authentication_token!' do
    it 'generates a unique token' do
      allow(Devise).to receive(:friendly_token).and_return('abc12345678')
      user.generate_authentication_token!

      expect(user.auth_token).to eq('abc12345678')      
    end

    it 'generates another auth token when the current auth token has already been taken' do
      allow(Devise).to receive(:friendly_token).and_return('abc12345678xyz', 'abc12345678xyz', 'abcXYZ123')
      existing_user = create(:user, auth_token: 'abc12345678xyz')
      user.generate_authentication_token!
      expect(user.auth_token).not_to eq(existing_user.auth_token)
    end
  end
  
end
